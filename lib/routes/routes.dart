import 'package:portfolio_flutter/screens/about.dart';
import 'package:portfolio_flutter/screens/contact.dart';
import 'package:portfolio_flutter/screens/education.dart';
import 'package:portfolio_flutter/screens/experience.dart';
import 'package:portfolio_flutter/screens/home.dart';
import 'package:portfolio_flutter/screens/services.dart';
import 'package:portfolio_flutter/screens/skills.dart';
import 'package:portfolio_flutter/screens/work.dart';

class Routes {
  static const String home = HomePage.routeName;
  static const String about = AboutPage.routeName;
  static const String services = ServicesPage.routeName;
  static const String skills = SkillsPage.routeName;
  static const String education = EducationPage.routeName;
  static const String experience = ExperiencePage.routeName;
  static const String work = WorkPage.routeName;
  static const String contact = ContactPage.routeName;
}
