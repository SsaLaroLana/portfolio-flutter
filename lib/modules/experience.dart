class Experience {
  final String hexColor;
  final String title;
  final String date;
  final String text;

  Experience({this.hexColor, this.title, this.date, this.text});

  factory Experience.fromJson(Map<String, dynamic> json) => Experience(
      hexColor: json['hex_color'],
      title: json['title'],
      date: json['date'],
      text: json['text']);
}
