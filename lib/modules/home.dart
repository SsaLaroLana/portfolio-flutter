class Home {
  final String titleOne;
  final String subTitleOne;
  final String buttonOne;

  final String titleTwo;
  final String subTitleTwo;
  final String buttonTwo;

  Home(
      {this.titleOne,
      this.subTitleOne,
      this.buttonOne,
      this.titleTwo,
      this.subTitleTwo,
      this.buttonTwo});

  factory Home.fromJson(Map<String, dynamic> json) => Home(
      titleOne: json['title_one'],
      subTitleOne: json['subtitle_one'],
      buttonOne: json['button_one'],
      titleTwo: json['title_two'],
      subTitleTwo: json['subtitle_two'],
      buttonTwo: json['button_two']);
}
