class Triple<X, Y, Z> {
  X first;
  Y second;
  Z third;

  Triple(X first, Y second, Z third) {
    this.first = first;
    this.second = second;
    this.third = third;
  }
}
