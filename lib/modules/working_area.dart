class WorkingArea {
  final String iconUrl;
  final String title;
  final String hexColor;

  WorkingArea({this.iconUrl, this.title, this.hexColor});

  factory WorkingArea.fromJson(Map<String, dynamic> json) => WorkingArea(
      iconUrl: json['icon_url'],
      title: json['title'],
      hexColor: json['hex_color']);
}
