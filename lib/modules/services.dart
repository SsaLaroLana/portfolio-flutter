import 'package:portfolio_flutter/modules/service.dart';

class Services {
  final String title;
  final List<Service> services;

  Services({this.title, this.services});

  factory Services.fromJson(Map<String, dynamic> json) => Services(
      title: json['title'],
      services: (json['services'] as Iterable)
          .map((service) => Service.fromJson(service))
          .toList());
}
