import 'package:portfolio_flutter/modules/working_area.dart';

class About {
  final String title;
  final String text;
  final List<WorkingArea> workingAreas;

  About({this.title, this.text, this.workingAreas});

  factory About.fromJson(Map<String, dynamic> json) => About(
      title: json['title'],
      text: json['text'],
      workingAreas: (json['working_areas'] as Iterable)
          .map((workingArea) => WorkingArea.fromJson(workingArea))
          .toList());
}
