import 'package:portfolio_flutter/modules/work.dart';

class Works {
  final String title;
  final String footer;
  final List<Work> works;

  Works({this.title, this.footer, this.works});

  factory Works.fromJson(Map<String, dynamic> json) => Works(
      title: json['title'],
      footer: json['footer'],
      works: (json['works'] as Iterable)
          .map((work) => Work.fromJson(work))
          .toList());
}
