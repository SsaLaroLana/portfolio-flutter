class Work {
  final String name;
  final String androidUrl;
  final String iosUrl;
  final String type;
  final String backgroundImageUrl;

  Work(
      {this.name,
      this.androidUrl,
      this.iosUrl,
      this.type,
      this.backgroundImageUrl});

  factory Work.fromJson(Map<String, dynamic> json) => Work(
      name: json['name'],
      androidUrl: json['android_url'],
      iosUrl: json['ios_url'],
      type: json['type'],
      backgroundImageUrl: json['background_image_url']);
}
