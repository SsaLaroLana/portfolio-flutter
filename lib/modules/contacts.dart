import 'package:portfolio_flutter/modules/contact.dart';

class Contacts {
  final String title;
  final String text;
  final List<Contact> contacts;

  Contacts({this.title, this.text, this.contacts});

  factory Contacts.fromJson(Map<String, dynamic> json) => Contacts(
      title: json['title'],
      text: json['text'],
      contacts: (json['contacts'] as Iterable)
          .map((contact) => Contact.fromJson(contact))
          .toList());
}
