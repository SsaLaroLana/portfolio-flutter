import 'package:portfolio_flutter/modules/education.dart';

class Educations {
  final String title;
  final List<Education> educations;

  Educations({this.title, this.educations});

  factory Educations.fromJson(Map<String, dynamic> json) => Educations(
      title: json['title'],
      educations: (json['educations'] as Iterable)
          .map((education) => Education.fromJson(education))
          .toList());
}
