class Contact {
  final String iconUrl;
  final String name;
  final String contactUrl;

  Contact({this.iconUrl, this.name, this.contactUrl});

  factory Contact.fromJson(Map<String, dynamic> json) => Contact(
      iconUrl: json['icon_url'],
      name: json['name'],
      contactUrl: json['contact_url']);
}
