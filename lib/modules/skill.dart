class Skill {
  final String name;
  final int value;
  final String hexColor;

  Skill({this.name, this.value, this.hexColor});

  factory Skill.fromJson(Map<String, dynamic> json) => Skill(
      name: json['name'], value: json['value'], hexColor: json['hex_color']);
}
