import 'package:portfolio_flutter/modules/skill.dart';

class Skills {
  final String title;
  final String text;
  final List<Skill> skills;

  Skills({this.title, this.text, this.skills});

  factory Skills.fromJson(Map<String, dynamic> json) => Skills(
      title: json['title'],
      text: json['text'],
      skills: (json['skills'] as Iterable)
          .map((skill) => Skill.fromJson(skill))
          .toList());
}
