class Service {
  final String iconUrl;
  final String hexColor;
  final String title;
  final String text;

  Service({this.iconUrl, this.hexColor, this.title, this.text});

  factory Service.fromJson(Map<String, dynamic> json) => Service(
      iconUrl: json['icon_url'],
      hexColor: json['hex_color'],
      title: json['title'],
      text: json['text']);
}
