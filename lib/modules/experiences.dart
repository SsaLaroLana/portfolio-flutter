import 'package:portfolio_flutter/modules/experience.dart';

class Experiences {
  final String title;
  final List<Experience> experiences;

  Experiences({this.title, this.experiences});

  factory Experiences.fromJson(Map<String, dynamic> json) => Experiences(
      title: json['title'],
      experiences: (json['experiences'] as Iterable)
          .map((experience) => Experience.fromJson(experience))
          .toList());
}
