class Education {
  final String title;
  final String text;

  Education({this.title, this.text});

  factory Education.fromJson(Map<String, dynamic> json) =>
      Education(title: json['title'], text: json['text']);
}
