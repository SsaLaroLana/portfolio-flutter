import 'package:portfolio_flutter/modules/about.dart';
import 'package:portfolio_flutter/modules/contacts.dart';
import 'package:portfolio_flutter/modules/educations.dart';
import 'package:portfolio_flutter/modules/experiences.dart';
import 'package:portfolio_flutter/modules/home.dart';
import 'package:portfolio_flutter/modules/services.dart';
import 'package:portfolio_flutter/modules/skills.dart';
import 'package:portfolio_flutter/modules/works.dart';

class Cv {
  final Home home;
  final About about;
  final Services services;
  final Skills skills;
  final Educations educations;
  final Experiences experiences;
  final Works works;
  final Contacts contacts;

  Cv(
      {this.home,
      this.about,
      this.services,
      this.skills,
      this.educations,
      this.experiences,
      this.works,
      this.contacts});

  factory Cv.fromJson(Map<String, dynamic> json) => Cv(
      home: Home.fromJson(json['home']),
      about: About.fromJson(json['about']),
      services: Services.fromJson(json['services']),
      skills: Skills.fromJson(json['skills']),
      educations: Educations.fromJson(json['educations']),
      experiences: Experiences.fromJson(json['experiences']),
      works: Works.fromJson(json['works']),
      contacts: Contacts.fromJson(json['contacts']));
}
