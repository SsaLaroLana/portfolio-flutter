import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:portfolio_flutter/modules/cv.dart';

class Sync {
  static Future<Cv> fetchCv() async {
    final response = await http.get('http://www.baldaz.it/cv.json');

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Cv.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }
}
