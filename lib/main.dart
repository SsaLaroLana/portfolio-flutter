import 'package:flutter/material.dart';
import 'package:portfolio_flutter/routes/routes.dart';
import 'package:portfolio_flutter/screens/about.dart';
import 'package:portfolio_flutter/screens/contact.dart';
import 'package:portfolio_flutter/screens/education.dart';
import 'package:portfolio_flutter/screens/experience.dart';
import 'package:portfolio_flutter/screens/home.dart';
import 'package:portfolio_flutter/screens/services.dart';
import 'package:portfolio_flutter/screens/skills.dart';
import 'package:portfolio_flutter/screens/work.dart';

void main() => runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: Routes.home,
        routes: {
          Routes.home: (context) => HomePage(),
          Routes.about: (context) => AboutPage(),
          Routes.services: (context) => ServicesPage(),
          Routes.skills: (context) => SkillsPage(),
          Routes.education: (context) => EducationPage(),
          Routes.experience: (context) => ExperiencePage(),
          Routes.work: (context) => WorkPage(),
          Routes.contact: (context) => ContactPage()
        },
      ),
    );
