import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/contact.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/screens/core/core_page.dart';
import 'package:portfolio_flutter/utils/utils.dart';

class ContactPage extends CorePage {
  static const String routeName = '/contact';

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends CorePageState<ContactPage> {
  ///
  /// Implementing Body of my Contacts section
  ///
  @override
  Widget makeBody(Cv cv) => Scaffold(
      backgroundColor: Colors.blueGrey.shade800,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Text('Contact'),
      ),
      body: ListView.builder(
          itemCount: cv.contacts.contacts.length,
          itemBuilder: (context, int index) =>
              _makeContact(cv.contacts.contacts[index])));

  ///
  /// Prepare Contact element to show in a list
  ///
  InkWell _makeContact(Contact contact) => InkWell(
        onTap: () => openUrl(contact.contactUrl),
        child: Container(
          margin: EdgeInsets.all(20.0),
          height: 100,
          width: 100,
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(30.0),
                margin: EdgeInsets.only(right: 20.0),
                color: Colors.grey.shade200,
                child: Image.network(
                  contact.iconUrl,
                  fit: BoxFit.contain,
                  color: HexColor.fromHex('#2c98f0'),
                ),
              ),
              Text(contact.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white))
            ],
          ),
        ),
      );
}
