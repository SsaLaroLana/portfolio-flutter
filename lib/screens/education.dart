import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/modules/education.dart';
import 'package:portfolio_flutter/screens/core/core_page.dart';

class EducationPage extends CorePage {
  static const String routeName = '/education';

  @override
  _EducationPageState createState() => _EducationPageState();
}

class _EducationPageState extends CorePageState<EducationPage> {
  ///
  /// Implementing Educations body with a ExpandableListview
  ///
  @override
  Widget makeBody(Cv cv) => Scaffold(
      backgroundColor: Colors.blueGrey.shade800,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Text('Education'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text('EDUCATION',
                  style: TextStyle(
                      color: Colors.white,
                      fontStyle: FontStyle.italic,
                      fontSize: 18)),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(cv.educations.title,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 22)),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: cv.educations.educations.length,
                  itemBuilder: (context, int index) =>
                      _makeEducation(cv.educations.educations[index])),
            )
          ],
        ),
      ));

  ///
  /// Rendering Education Card in expandable mode
  ///
  Card _makeEducation(Education education) => Card(
        margin: EdgeInsets.symmetric(vertical: 8.0),
        child: ExpansionTile(
          initiallyExpanded: education.text.isNotEmpty,
          title: Text(education.title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.black)),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(education.text, style: TextStyle(fontSize: 16)),
            )
          ],
        ),
      );
}
