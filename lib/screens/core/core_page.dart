import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/network/sync.dart';

abstract class CorePage extends StatefulWidget {}

abstract class CorePageState<X extends CorePage> extends State<X> {
  Future<Cv> cv;

  @override
  void initState() {
    super.initState();
    cv = Sync.fetchCv();
  }

  @override
  Widget build(BuildContext context) => FutureBuilder<Cv>(
        future: this.cv,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return makeBody(snapshot.data);
          } else if (snapshot.hasError) {
            return Center(child: Text('${snapshot.error}'));
          }
          return Center(child: CircularProgressIndicator());
        },
      );

  Widget makeBody(Cv cv);
}
