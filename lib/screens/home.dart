import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/modules/triple.dart';
import 'package:portfolio_flutter/routes/routes.dart';
import 'package:portfolio_flutter/screens/core/core_page.dart';
import 'package:portfolio_flutter/screens/skills.dart';
import 'package:portfolio_flutter/utils/utils.dart';

class HomePage extends CorePage {
  static const String routeName = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends CorePageState<HomePage> {
  ButtonStyle _outlinedButtonStyle;

  @override
  void initState() {
    _outlinedButtonStyle = OutlinedButton.styleFrom(
        primary: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 16),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(2)),
        ),
        side: BorderSide(color: Colors.white));
    super.initState();
  }

  final List<Triple<IconData, String, String>> _items = [
    new Triple(Icons.info, 'About', Routes.about),
    new Triple(Icons.room_service, 'Services', Routes.services),
    new Triple(Icons.language, 'Skills', Routes.skills),
    new Triple(Icons.school, 'Education', Routes.education),
    new Triple(Icons.show_chart, 'Experience', Routes.experience),
    new Triple(Icons.work, 'Work', Routes.work),
    new Triple(Icons.contact_mail, 'Contact', Routes.contact)
  ];

  ///
  /// Build home body after sync successful
  ///
  @override
  DefaultTabController makeBody(Cv cv) => DefaultTabController(
        child: Scaffold(
          backgroundColor: Colors.blueGrey.shade800,
          appBar: AppBar(
            backgroundColor: Colors.blueGrey.shade900,
            title: Text('Home'),
            bottom: TabBar(
              tabs: <Widget>[
                Padding(
                    padding: EdgeInsets.only(bottom: 8),
                    child: Icon(Icons.desktop_mac)
                ),
                Padding(
                    padding: EdgeInsets.only(bottom: 8),
                    child: Icon(Icons.smartphone)
                )
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      cv.home.titleTwo,
                      style: TextStyle(color: Colors.white, fontSize: 35),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      child: Text(cv.home.subTitleTwo + ' 🤣',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 18)),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: OutlinedButton(
                        style: _outlinedButtonStyle,
                        child: Text(
                          cv.home.buttonTwo.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        onPressed: () => openUrl(
                            'http://www.baldaz.it/CV%20-%20Marco%20Baldazzi.pdf'),
                      ),
                    )
                  ],
                ),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      cv.home.titleOne,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 35),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      child: Text(cv.home.subTitleOne,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 18)),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: OutlinedButton(
                        style: _outlinedButtonStyle,
                        child: Text(
                          cv.home.buttonOne.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        onPressed: () => Navigator.of(context)
                            .pushNamed(SkillsPage.routeName),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          drawer: _makeDrawer(),
        ),
        length: 2,
        initialIndex: 0,
      );

  ///
  /// Build drawer menu for handle all CV sections
  ///
  Drawer _makeDrawer() => Drawer(
        child: Container(
          color: Colors.blueGrey.shade800,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: 200.0,
                child: UserAccountsDrawerHeader(
                  margin: EdgeInsets.symmetric(horizontal: 8.0),
                  decoration: BoxDecoration(color: Colors.transparent),
                  accountName: Text('Marco B.'),
                  accountEmail: Text('baldmarc@alice.it'),
                  currentAccountPicture: CircleAvatar(
                    backgroundColor: Colors.blueGrey.shade900,
                    radius: 60,
                    backgroundImage: AssetImage('images/me.png'),
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: _items.length,
                    itemBuilder: (context, int index) =>
                        _makeCard(_items[index])),
              ),
            ],
          ),
        ),
      );

  ///
  /// Build Drawer Menu items
  ///
  Card _makeCard(Triple<IconData, String, String> item) => Card(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        color: Colors.blueGrey.shade900,
        child: ListTile(
          leading: Icon(
            item.first,
            color: Colors.white,
          ),
          title: Text(
            item.second,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
          ),
          onTap: () => Navigator.of(context).pushNamed(item.third),
        ),
      );

  ///
  ///
  ///
}
