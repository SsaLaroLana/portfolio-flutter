import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/modules/working_area.dart';
import 'package:portfolio_flutter/screens/core/core_page.dart';

class AboutPage extends CorePage {
  static const String routeName = '/about';

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends CorePageState<AboutPage> {
  ///
  /// Implementing Scaffold for About Page
  ///
  ///
  @override
  Scaffold makeBody(Cv cv) => Scaffold(
      backgroundColor: Colors.blueGrey.shade800,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Text('About'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(
                      'ABOUT ME',
                      style: TextStyle(
                          color: Colors.white,
                          fontStyle: FontStyle.italic,
                          fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(cv.about.title,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 22)),
                  ),
                  Text(cv.about.text,
                      style: TextStyle(
                          color: Colors.white, fontSize: 18, height: 1.5)),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                child: Column(
                    children: List.generate(
                        cv.about.workingAreas.length,
                        (index) =>
                            _makeWorkingArea(cv.about.workingAreas[index]))),
              )
            ],
          ),
        ),
      ));

  ///
  /// Make Working Area elements for showing up in list
  ///
  Card _makeWorkingArea(WorkingArea workingArea) => Card(
        margin: EdgeInsets.symmetric(vertical: 8.0),
        child: ListTile(
          title: Text(workingArea.title),
        ),
      );
}
