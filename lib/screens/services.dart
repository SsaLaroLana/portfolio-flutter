import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/modules/service.dart';
import 'package:portfolio_flutter/screens/core/core_page.dart';

class ServicesPage extends CorePage {
  static const String routeName = '/services';

  @override
  _ServicePageState createState() => _ServicePageState();
}

class _ServicePageState extends CorePageState<ServicesPage> {
  Future<Cv> cv;

  ///
  /// Handle List of services with Card and section Title
  ///
  @override
  Widget makeBody(Cv cv) => Scaffold(
      backgroundColor: Colors.blueGrey.shade800,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Text('Services'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    'WHAT I DO?',
                    style: TextStyle(
                        color: Colors.white,
                        fontStyle: FontStyle.italic,
                        fontSize: 18),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(cv.services.title,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 22)),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0),
              child: Column(
                children: List.generate(cv.services.services.length,
                    (index) => _makeService(cv.services.services[index])),
              ),
            )
          ],
        ),
      ));

  ///
  /// Render Service with a Card and relative data
  ///
  Widget _makeService(Service service) => Card(
        margin: EdgeInsets.symmetric(vertical: 4.0),
        child: ListTile(
          isThreeLine: true,
          leading: Image.network(
            service.iconUrl,
            height: 40,
            fit: BoxFit.fitWidth,
          ),
          title: Padding(
            padding: const EdgeInsets.only(bottom: 4.0),
            child: Text(
              service.title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 4.0, bottom: 8.0),
            child: Text(
              service.text,
              style: TextStyle(fontSize: 16),
            ),
          ),
        ),
      );
}
