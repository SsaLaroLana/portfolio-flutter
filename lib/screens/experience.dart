import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/modules/experience.dart';
import 'package:portfolio_flutter/screens/core/core_page.dart';
import 'package:portfolio_flutter/utils/utils.dart';

class ExperiencePage extends CorePage {
  static const String routeName = '/experience';

  _ExperiencePageState createState() => _ExperiencePageState();
}

class _ExperiencePageState extends CorePageState<ExperiencePage> {
  ///
  /// Implementing Work Experiences body
  ///
  @override
  Scaffold makeBody(Cv cv) => Scaffold(
      backgroundColor: Colors.blueGrey.shade800,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Text('Experience'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'EXPERIENCE',
                style: TextStyle(
                    color: Colors.white,
                    fontStyle: FontStyle.italic,
                    fontSize: 18),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                cv.experiences.title,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: cv.experiences.experiences.length,
                itemBuilder: (context, int index) =>
                    _makeExperience(cv.experiences.experiences[index]),
              ),
            )
          ],
        ),
      ));

  ///
  /// Rendering my Work Experience in a beautiful way
  ///
  Stack _makeExperience(Experience experience) => Stack(
        children: <Widget>[
          Card(
            margin: EdgeInsets.fromLTRB(15, 20, 0, 8),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 30.0),
                    child: AutoSizeText(
                      experience.title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30.0),
                    child: Text(
                      experience.date,
                      maxLines: 1,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20.0),
                    child:
                        Text(experience.text, style: TextStyle(fontSize: 16)),
                  )
                ],
              ),
            ),
          ),
          Material(
            elevation: 4.0,
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: HexColor.fromHex(experience.hexColor),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.create,
                size: 30,
              ),
            ),
          ),
        ],
      );
}
