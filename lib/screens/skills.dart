import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/modules/skill.dart';
import 'package:portfolio_flutter/screens/core/core_page.dart';
import 'package:portfolio_flutter/utils/utils.dart';

class SkillsPage extends CorePage {
  static const String routeName = '/skills';

  @override
  _SkillsPageState createState() => _SkillsPageState();
}

class _SkillsPageState extends CorePageState<SkillsPage> {
  ///
  /// Implementing ScrollView with Title, Text and list of Skills
  ///
  @override
  Widget makeBody(Cv cv) => Scaffold(
      backgroundColor: Colors.blueGrey.shade800,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Text('Skills'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text('MY SPECIALTY',
                        style: TextStyle(
                            color: Colors.white,
                            fontStyle: FontStyle.italic,
                            fontSize: 18)),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(cv.skills.title,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 22)),
                  ),
                  Text(
                    cv.skills.text,
                    style: TextStyle(
                        color: Colors.white, fontSize: 18, height: 1.5),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                child: Column(
                  children: List.generate(
                    cv.skills.skills.length,
                    (index) => _makeSkill(cv.skills.skills[index]),
                  ),
                ),
              )
            ],
          ),
        ),
      ));

  ///
  /// Rendering Skill Card with all data
  ///
  Card _makeSkill(Skill skill) => Card(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(skill.name,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: SliderTheme(
                      data: SliderThemeData(
                          showValueIndicator: ShowValueIndicator.always,
                          thumbShape:
                              RoundSliderThumbShape(enabledThumbRadius: 10)),
                      child: Slider(
                        value: skill.value.toDouble(),
                        min: 0,
                        max: 100,
                        label: skill.value.toString() + '%',
                        activeColor: HexColor.fromHex(skill.hexColor),
                        onChanged: (double value) {},
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
}
