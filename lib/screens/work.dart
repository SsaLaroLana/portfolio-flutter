import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:portfolio_flutter/modules/cv.dart';
import 'package:portfolio_flutter/modules/work.dart';
import 'package:portfolio_flutter/utils/utils.dart';

import 'core/core_page.dart';

class WorkPage extends CorePage {
  static const String routeName = '/work';

  @override
  _WorkPageState createState() => _WorkPageState();
}

class _WorkPageState extends CorePageState<WorkPage> {
  ///
  /// Implementing Body for showing my Works
  ///
  @override
  Widget makeBody(Cv cv) => Scaffold(
      backgroundColor: Colors.blueGrey.shade800,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Text('Works'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: GridView.count(
            crossAxisCount: 2,
            children: List.generate(cv.works.works.length,
                (index) => _makeWork(cv.works.works[index]))),
      ));

  ///
  /// Implementing Work Card for rendering my works
  ///
  InkWell _makeWork(Work work) => InkWell(
      borderRadius: BorderRadius.circular(5),
      onTap: () {},
      child: Card(
        child: Stack(
          children: <Widget>[
            Image.network(
              work.backgroundImageUrl,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        AutoSizeText(
                          work.name,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.white),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          child: Text(work.type,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white)),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.android),
                          onPressed: () => openUrl(work.androidUrl),
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(FontAwesome.apple),
                          onPressed: () => openUrl(work.iosUrl),
                        )
                      ],
                    )
                  ]),
            ),
          ],
        ),
      ));
}
